<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Etapa;
use app\models\Equipo;
use app\models\Puerto;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
 /*MOSTRAR ESTADÍSTICAS GENERALES*/
      public function actionGenerales()
    {
        $dataProvider = new ActiveDataProvider([
              'query' => Ciclista::find() ,
          ]);
        
 $grafNumerociclistas = Yii::$app->db->createCommand('SELECT nomequipo, COUNT(*) numero FROM ciclista GROUP BY nomequipo')->queryAll();
          $grafCiclistasetapasganadas = Yii::$app->db->createCommand('SELECT DISTINCT nombre, COUNT(*)numero2 FROM ciclista INNER JOIN etapa USING (dorsal) GROUP BY dorsal;')->queryAll();
          $grafCiclistaspuertosganados = Yii::$app->db->createCommand('SELECT DISTINCT nombre, COUNT(*)numero3 FROM ciclista  INNER JOIN puerto USING (dorsal) GROUP BY dorsal;')->queryAll();
     
 
          /*KMS por etapas*/
          $kmsEtapas= yii:: $app -> db -> createCommand('Select numetapa etapas, kms recorrido from etapa') -> queryAll();
          
                /*Maillot más llevado*/
          $maillotsUsados= yii:: $app -> db -> createCommand('Select color, código, COUNT(*) numero4 from lleva inner join maillot using(código) group by código ') -> queryAll();

          
          
                /*Ciclista que ha ganado más etapas*/
             $ciclistaMasEstapas=yii:: $app -> db -> createCommand('SELECT nombre ganador , dorsal, COUNT(*) total FROM ciclista 
INNER JOIN etapa USING(dorsal) 
GROUP BY dorsal 
HAVING COUNT(*) = 
(SELECT MAX(total) FROM (SELECT dorsal, COUNT(*) total FROM etapa GROUP BY dorsal )c1)  ') ->queryALL();
          
                /*Ciclista que ha ganado más puertos*/
        $ciclistaMasPuertos=yii:: $app -> db -> createCommand('SELECT nombre ganadorPuerto , dorsal, COUNT(*) total1 FROM ciclista 
INNER JOIN puerto USING(dorsal) 
GROUP BY dorsal 
HAVING COUNT(*) = 
(SELECT MAX(total1) FROM (SELECT dorsal, COUNT(*) total1 FROM puerto GROUP BY dorsal )c1) ') ->queryALL();
        
        
                /*Altura de los puerto*/
          $alturaPuerto= yii:: $app -> db -> createCommand('Select nompuerto nombrePuerto, altura from puerto') -> queryAll();
     
          /*edad media de los ciclistas*/
        
        $edadMedia=yii::$app->db->createCommand('Select AVG(edad) edadMedia from ciclista')->queryALL();
        
        /*etapas circulares*/
        $etapasCirculares=yii::$app->db->createCommand('Select numetapa numeroEtapa, salida from etapa where salida=llegada')->queryALL();
        
        /*victorias de etapa por equipos*/
        $victoriaEquipos=yii::$app->db->createCommand('SELECT nomequipo, COUNT(*) victorias FROM ciclista INNER JOIN etapa  USING(dorsal) GROUP BY nomequipo')->queryALL();
        
         /*victorias puerto por equipos*/
        $victoriaEquiposPuerto=yii::$app->db->createCommand('SELECT nomequipo, COUNT(*) victoriasPuertos FROM ciclista INNER JOIN puerto  USING(dorsal) GROUP BY nomequipo')->queryALL();
        
        /*Edad de los ciclistas*/
        $edadCiclistas=yii::$app->db->createCommand('Select distinct edad, count(*) edadCiclista from ciclista group by edad')->queryALL();
          
        
        /*Premio maillots*/
        $premiosMaillot=yii::$app->db->createCommand('Select premio, código from maillot')->queryALL();
        return $this->render('general',[
            
          "resultado"=>$dataProvider,
            'grafNumerociclistas'=>$grafNumerociclistas,
           'grafCiclistasetapasganadas'=>$grafCiclistasetapasganadas,
           'grafCiclistaspuertosganados'=>$grafCiclistaspuertosganados,
            'ciclistaMasEstapas'=> $ciclistaMasEstapas,
            'ciclistaMasPuertos'=>$ciclistaMasPuertos,
            'edadMedia'=>$edadMedia,
            'kmsEtapas'=>$kmsEtapas,
            'maillotsUsados'=>$maillotsUsados,
            'alturaPuerto' =>$alturaPuerto,
            'etapasCirculares'=>$etapasCirculares,
            'edadCiclistas'=> $edadCiclistas,
            'premiosMaillot'=>$premiosMaillot,
            'victoriaEquipos'=>$victoriaEquipos,
            'victoriaEquiposPuerto'=>$victoriaEquiposPuerto,
     ]);
          

        
    }
            
        /*MOstrar información de los equipos*/
      public function actionEquipos()
    {
          $dataProvider = new ActiveDataProvider([
              'query' => Equipo::find() ,
          ]);
        return $this->render('equipos',[
            'dataProvider' => $dataProvider,
            

        ]);
    }


     /*ESTADÍSTICAS Por cada equipo*/
      public function actionEquiposciclistas($nomequipo)
    {
          $dataProvider = new ActiveDataProvider([
              'query' => Ciclista::find() -> where("nomequipo= '$nomequipo' "),
          ]);
          
               /*Mostrar número de etapas  ganados*/
            $ciclistasPorEquipo =  Ciclista:: find() -> select('nombre') -> where("nomequipo= ' $nomequipo' ")->scalar();
            
    return $this->render('equiposciclista',[
           'equiposciclistas'=>$dataProvider,
        'ciclistasPorEquipo'=>$ciclistasPorEquipo,
       
   
        ]);
    }
    
        /*MOSTRAR ESTADÍSTICAS Ciclista*/
      public function actionCiclistas()
    {
          $dataProvider = new ActiveDataProvider([
              'query' => Ciclista::find() ,
          ]);
          
        return $this->render('ciclistas',[
            "dataProvider" => $dataProvider,

        ]);
    }
     /*ESTADÍSTICAS Por cada ciclista*/
      public function actionEstadisticas($dorsal)
    {
           $dataProvider = new ActiveDataProvider([
              'query' => Ciclista::find() -> where("dorsal= '$dorsal' "),
          ]);
           
                /*Mostrar número de etapas  ganados*/
            $totales =  Etapa:: find() -> select('count(*) total') -> where("dorsal= ' $dorsal' ")->scalar();
            
            /*Mostrar número de puertos ganados*/
            $puertos = Puerto:: find() -> select('count(*) total') -> where("dorsal= ' $dorsal' ")->scalar();
            
                 /*Mostrar número de maillots ganados*/
             $maillots = Lleva:: find() -> select('count(*) total') -> where("dorsal= ' $dorsal' ")->scalar();
             
                  /*Mostrar el total de los premios ganados*/
            $query = new \yii\db\Query;
            $query  -> select('sum(premio) cantidad') ->from ('lleva')  ->join('INNER join', 'maillot', 'maillot.código=lleva.código') -> where("dorsal= ' $dorsal' ") -> groupby('dorsal');
            $premio = $query->scalar();
          	
                
              $premio  = ($premio  == null) ? 0 : $premio ;
              
 
       
          
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "totales" => $totales,
            "puertos" => $puertos,
            "maillots"=>$maillots,
            'premio'=>$premio,
           
   
        ]);
    }
    
 
    
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
     public function actionInformacion()
    {
        return $this->render('informacion');
    }
}
