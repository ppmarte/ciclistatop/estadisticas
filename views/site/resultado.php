<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
 $this->title= 'Ciclistas';

?>


<div class="jumbotron">
    <?=   ListView::widget([
            'dataProvider' => $resultados,
           'itemView' => '_resultado',
          'layout'=>"{items}",
          'viewParams' => ['totales' => $totales, 'puertos'=>$puertos, 'maillots'=>$maillots, 'premio'=>$premio,  ],

        ]);
?>
    
  
    </div>
