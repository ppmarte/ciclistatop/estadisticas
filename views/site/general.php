<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use  yii\widgets\ListView;
use yii\web\JsExpression;
use yii\base\BaseObjec;
$this->title = 'General';
 /*$this->params['breadcrumbs'][] = $this->title;            */
           
?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
   

  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel"  data-interval="false">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" ></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
     
      <div class="carousel-caption d-none d-md-block">
       
        

<?php 
      foreach($grafNumerociclistas as $values){
                        $nomequipo[] = ($values['nomequipo']);
                        $numero[] = intval($values['numero']);
                    }
                    echo
                    
                    Highcharts::widget([
                        'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'column'],
                            'title' => ['text' => 'Equipo'],
                             'credits' => ['enabled' => false],
                            'xAxis' => ['categories' => $nomequipo],
                            'yAxis' => ['title' => ['text' => 'Ciclistas']],
                            'series' => [
                                [
                                   'name' => 'Número de ciclistas por equipo',
                                   'color' => ' #066361',
                                   'colorByPoint' => false,
                                   'data' => $numero,
                                ],
                            ],
                        ],
                    ]);

?>
      </div>
    </div>
    <div class="carousel-item">

      <div class="carousel-caption d-none d-md-block">
        <?php 
      foreach($grafCiclistasetapasganadas as $values){
                        $nombre[] = ($values['nombre']);
                        $numero2[] = intval($values['numero2']);
                    }
                    echo
                    
                    Highcharts::widget([
                        'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'column'],
                            'title' => ['text' => 'Etapas'],
                             'credits' => ['enabled' => false],
                            'xAxis' => ['categories' => $nombre],
                            'yAxis' => ['title' => ['text' => 'Número de victorias']],
                            'series' => [
                                [
                                   'name' => 'Etapas ganadas por cada ciclista',
                                   'color' => ' #066361 ',
                                   'colorByPoint' => false,
                                   'data' => $numero2,
                                ],
                            ],
                        ],
                    ]);

?>
      </div>
    </div>
    <div class="carousel-item">

      <div class="carousel-caption d-none d-md-block">
        <?php 
      foreach($grafCiclistaspuertosganados as $values){
                        $nombre[] = ($values['nombre']);
                        $numero3[] = intval($values['numero3']);
                    }
                    echo

                    Highcharts::widget([
                        'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'column'],
                            'title' => ['text' => 'Ciclistas'],
                            'credits'=>[
        'enabled'=> false
    ],
                            'xAxis' => ['categories' => $nombre],
                            'yAxis' => ['title' => ['text' => 'Numero de victorias']],
                            'series' => [
                                [
                                   'name' => 'Puertos ganados por cada ciclista',
                                   'color' => '#066361 ',
                                   'colorByPoint' => false,
                                   'data' => $numero3,
                                ],
                            ],
                        ],
                    ]);

?>
      </div>
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-target="#carouselExampleCaptions" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </button>
</div>



<div class="body-content">
    <div class="row">

    
    <div class="body-content">
    <div class="row">
        <div class="col-sm-4">
          <div class="card charminimo">
            <div class="card-body">
            
                <?php 
      foreach($kmsEtapas as $values){
                        $data_name[] =  ($values['etapas']);
                        $data_kms[] = intval($values['recorrido']);
                        $name[] = ($values['etapas']);
                     
                    }
                   	
                   
                    echo
  Highcharts::widget([
                        'scripts' => ['module','themes/dark-unica', ],
                        'options' => [
                            'chart' => ['type' => 'line'],
                            'title' => ['text' => 'Kms de las etapas'],
                             'credits' => ['enabled' => false],
                             'tooltip' => [ 'valueSuffix'=> ' kms'],
                            'xAxis' => ['categories' => $data_name],
                            'yAxis' => ['title' => ['text' => ' Kilómetros']],
                            'series' => [
                                [
                                   'name' => 'Altura',
                                   'color' => ' #066361',
                                   'colorByPoint' => false,
                                   'data' => $data_kms,
                                ],
                            ],
                        ],
                    ]);


?>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
            <div class="card charminimo">
              <div class="card-body">
                   
                <?php 
      foreach($alturaPuerto as $values){
                        $puerto[] = ($values['nombrePuerto']);
                        $altura[] = intval($values['altura']);
                    }
                    echo
  Highcharts::widget([
                       'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'line'],
                            'title' => ['text' => 'Altura de los puertos'],
                             'credits' => ['enabled' => false],
                             'tooltip' => [ 'valueSuffix'=> ' metros'],
                            'xAxis' => ['categories' => $puerto],
                            'yAxis' => ['title' => ['text' => ' Metros']],
                            'series' => [
                                [
                                   'name' => 'Altura',
                                   'color' => ' #066361',
                                   'colorByPoint' => false,
                                   'data' => $altura,
                                ],
                            ],
                        ],
                    ]);

?>
                
              </div>
            </div>
         </div>
        
        
        <div class="col-sm-4">
            <div class="card charminimo">
              <div class="card-body">
                   <?php 
      foreach($maillotsUsados as $values){
                        $codigo[] = ($values['código']);
                        $numero4[] = intval($values['numero4']);
                    }
                    echo
Highcharts::widget([
                       'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'line'],
                            'title' => ['text' => 'Maillots Ganados'],
                             'credits' => ['enabled' => false],
                             'tooltip' => [ 'valueSuffix'=> ' ganados'],
                            'xAxis' => ['categories' => $codigo],
                            'yAxis' => ['title' => ['text' => ' Ganados']],
                            'series' => [
                                [
                                   'name' => 'Ganados',
                                   'color' => ' #066361',
                                   'colorByPoint' => false,
                                   'data' => $numero4,
                                ],
                            ],
                        ],
                    ]);

?>
               

              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card charminimo">
              <div class="card-body">
                   <?php 
      foreach($edadCiclistas as $values){
                        $edad[] = ($values['edad']);
                        $edadCiclista[] = intval($values['edadCiclista']);
                    }
                    echo
Highcharts::widget([
                       'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'line'],
                            'title' => ['text' => 'Edad de los ciclistas'],
                             'credits' => ['enabled' => false],
                             'tooltip' => [ 'valueSuffix'=> ' ciclistas'],
                            'xAxis' => ['categories' => $edad],
                            'yAxis' => ['title' => ['text' => ' Edad']],
                            'series' => [
                                [
                                   'name' => 'Tienen esa edad',
                                   'color' => ' #066361',
                                   'colorByPoint' => false,
                                   'data' => $edadCiclista,
                                ],
                            ],
                        ],
                    ]);
?>
              
              </div>
            </div>
         </div>
        
 
 

  <div class="col-sm-4">
            <div class="card charminimo">
              <div class="card-body">
                                   <?php 
      foreach($victoriaEquipos as $values){
                        $equip[] = ($values['nomequipo']);
                        $victoria[] = intval($values['victorias']);
                       
                    }
                   echo

  Highcharts::widget([
                       'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'line'],
                            'title' => ['text' => 'Etapas ganadas por quipos'],
                             'credits' => ['enabled' => false],
                            'xAxis' => ['categories' => $equip],
                            'yAxis' => ['title' => ['text' => ' Etapas ganadas']],
                            'series' => [
                                [
                                   'name' => 'Victorias',
                                   'color' => ' #066361',
                                   'colorByPoint' => false,
                                   'data' => $victoria,
                                ],
                            ],
                        ],
                    ]);

?>
           
              </div>
            </div>
          </div>
<div class="col-sm-4">
            <div class="card charminimo">
              <div class="card-body">
                                   <?php 
      foreach($victoriaEquiposPuerto as $values){
                        $equipos[] = ($values['nomequipo']);
                        $victoriaPuertos[] = intval($values['victoriasPuertos']);
                       
                    }
                   echo

  Highcharts::widget([
                       'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'line'],
                            'title' => ['text' => 'Puertos ganados por quipos'],
                             'credits' => ['enabled' => false],
                            'xAxis' => ['categories' => $equipos],
                            'yAxis' => ['title' => ['text' => ' Puertos ganados']],
                            'series' => [
                                [
                                   'name' => 'Victorias',
                                   'color' => ' #066361',
                                   'colorByPoint' => false,
                                   'data' => $victoriaPuertos,
                                ],
                            ],
                        ],
                    ]);

?>
           
              </div>
            </div>
          </div>
    </div>
        
</div>
 
 