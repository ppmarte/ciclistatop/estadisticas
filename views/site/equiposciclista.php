<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
 $this->title= 'Ciclistas';


?>


<div class="jumbotron">
    
    <?=   ListView::widget([
            'dataProvider' => $equiposciclistas,
           'itemView' => '_equiposciclistas',
          'layout'=>'{items}',
          'viewParams' => [  'ciclistasPorEquipo'=>$ciclistasPorEquipo],

        ]);
?>
   
  
    </div>
