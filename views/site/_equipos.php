<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;



?>


    
    <div class="col-sm-12">
            <div class="card minimo">
              <div class="card-body">
                 <h5 class="card-title"><?=$model -> nomequipo?></h5>
                 <p class="card-text">  Dir. <?=$model ->director?></p>
                 <?= Html::a('Ver más',['site/equiposciclistas', 'nomequipo'=>$model -> nomequipo, ], ['class' => 'btn btn-primary btn-block'] ) ?>
              </div>
            </div>
        </div>
