<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
$this->title = 'Información general';

?>

<div class="jumbotron">
    <h1><?=$this->title?></h1>
    
    <p>  La 49ª edición, la del adiós de Pedro DELGADO, comenzó con pocas posibilidades para los españoles, ya que Perico estaba en el final de su carrera deportiva y los corredores jóvenes todavía eran demasiado inexpertos para optar al triunfo final, por lo que el dedo de los entendidos señaló a los suizos Alex ZULLE y Tony ROMINGER como grandes favoritos para el triunfo final.</p>
        
    <p>ROMINGER demostró su fortaleza y desde la primera etapa de montaña, en Sierra Nevada, dejó claro quién era el hombre a batir. Atacó y ZULLE no fue capaz de seguirle, dejando ver que no estaba al mismo nivel que había exhibido el año anterior.</p>
        
    <p>Tras la contrarreloj disputada en Benidorm, ROMINGER aventajaba ya en cuatro minutos a Mikel ZARRABEITIA, segundo clasificado en la general.</p>
    
    <p>El único ataque serio que tuvo que soportar el suizo se produjo en la subida a Cerler, cuando ZARRABEITIA atacó a seis kilómetros del final, pero tuvo que desistir ante la fenomenal respuesta de ROMINGER, quien no estaba dispuesto a dejar escapar su tercer triunfo consecutivo en la ronda española.</p>
        
    <p>Las siguientes etapas transcurrieron por los mismos cauces. ROMINGER y ZARRABEITIA se mostraron intratables en los primeros puestos, ZULLE vio cómo aumentaba día a día su ventaja, mientras que DELGADO, lleno de experiencia y sabiduría, administró sus fuerzas para conseguir un lugar en el podio, objetivo que cumplió</p>
</div>