<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
 $this->title= 'Equipos';

?>


<div class="jumbotron ">
          <h1><?= $this->title?></h1>
                 
        <?=   ListView::widget([
            'dataProvider' => $dataProvider,
           'itemView' => '_equipos',
            'layout'=>"\n{items}{pager}",
           
        ]);
?>
          
      </div>
        