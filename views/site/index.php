<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
$this->title = 'Inicio';

?>

<div id="fondo">
    <h1>Vuelta Ciclista España</h1>
    
   
    <div class="container text-center">
  <div class="row justify-content-evenly align-items-center">
    <div class="col-7"> <h2>Información General</h2>
       <p>  <h6>EDICIÓN: 49; MADRID-VALLADOLID DEL 25/04/1994 AL 15/05/1994</h6></p>
          
     <p>  <?= Html::img('@web/img/cartel94.jpg', ['alt' => 'My logo']) ?> La 49ª edición, la del adiós de Pedro DELGADO, comenzó con pocas posibilidades para los españoles, ya que Perico estaba en el final de su carrera deportiva 
        y los corredores jóvenes todavía eran demasiado inexpertos para optar al triunfo final, por lo que el dedo de los entendidos señaló a los suizos Alex ZULLE y 
        Tony ROMINGER como grandes favoritos para el triunfo final.</p>
        <p> <?= Html::a('Ver más',['site/informacion'], ['class' => 'btn btn-info btn-lg'] ) ?></p>
    </div>
      <div class="col-1">    </div>
    <div class="col-4 justify-content-center">
      <p> <?= Html::a('Estadísticas Generales',['site/generales'], ['class' => 'btn btn-info btn-lg btn-block'] ) ?></p>
    <p> <?= Html::a('Estadísticas De los Ciclistas',['site/ciclistas'], ['class' => 'btn btn-info btn-lg btn-block'] ) ?></p>
     <p> <?= Html::a('Equipos de la Vuelta España',['site/equipos'], ['class' => 'btn btn-info btn-lg btn-block'] ) ?></p>
    </div>
    
  </div>
</div>
    <section>
        
    </section>
    
    

</div>


