<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
$this->title = 'Ciclistas';

?>


<div class="jumbotron ">
          <h1><?= $this->title?></h1>
                 
        <?=   ListView::widget([
            'dataProvider' => $dataProvider,
           'itemView' => '_ciclistas',
            'layout'=>"\n{items}{pager}",
            
        ]);
?>
          
      </div>
        
 